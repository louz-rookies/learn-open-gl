#ifndef __MODEL_H__
#define __MODEL_H__

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <vector>
#include <string>

#include "mesh.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#pragma comment (lib, "assimp-vc143-mt.lib")

GLuint TextureFromFile(const char* path, const std::string& directory);

class Model {
public:
	std::vector<Mesh> mMeshs;
	std::vector<Texture> mTexturesLoaded;
	std::string directory;

	Model(const std::string& path) {
		loadModel(path);
	}

	void Draw(const Shader& shader) {
		for (GLuint i = 0; i < mMeshs.size(); ++i) {
			mMeshs[i].Draw(shader);
		}
	}

private:
	void loadModel(const std::string& path) {
		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
		if (!scene || (scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE) || !scene->mRootNode) {
			std::cout << "error::assimp::" << importer.GetErrorString() << std::endl;
			return;
		}
		directory = path.substr(0, path.find_last_of('/'));
		processNode(scene->mRootNode, scene);
	}

	void processNode(aiNode* node, const aiScene* scene) {

		for (GLuint i = 0; i < node->mNumMeshes; ++i) {
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
			mMeshs.push_back(processMesh(mesh, scene));
		}

		for (GLuint i = 0; i < node->mNumChildren; ++i) {
			processNode(node->mChildren[i], scene);
		}

	}

	Mesh processMesh(aiMesh* mesh, const aiScene* scene) {
		std::vector<Vertex> vertices;
		std::vector<GLuint> indices;
		std::vector<Texture> textures;

		for (GLuint i = 0; i < mesh->mNumVertices; ++i) {
			glm::vec3 vect3;
			Vertex vertex;

			// positions
			vect3.x = mesh->mVertices[i].x;
			vect3.y = mesh->mVertices[i].y;
			vect3.z = mesh->mVertices[i].z;
			vertex.Position = vect3;
			// normals
			if (mesh->HasNormals()) {
				vect3.x = mesh->mNormals[i].x;
				vect3.y = mesh->mNormals[i].y;
				vect3.z = mesh->mNormals[i].z;
				vertex.Normal = vect3;
			}
			// texture
			if (mesh->mTextureCoords[0]) {
				glm::vec2 vect2;
				vect2.x = mesh->mTextureCoords[0][i].x;
				vect2.y = mesh->mTextureCoords[0][i].y;
				vertex.TexCoords = vect2;
			} else {
				vertex.TexCoords = glm::vec2(0.0f, 0.0f);
			}

			vertices.push_back(vertex);
		}

		for (GLuint i = 0; i < mesh->mNumFaces; ++i) {
			aiFace face = mesh->mFaces[i];
			for (GLuint j = 0; j < face.mNumIndices; ++j) {
				indices.push_back(face.mIndices[j]);
			}
		}

		if (mesh->mMaterialIndex >= 0) {
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
			std::vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
			textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

			std::vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
			textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		}

		return Mesh(vertices, indices, textures);
	}

	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName) {
		std::vector<Texture> textures;
		for (GLuint i = 0; i < mat->GetTextureCount(type); ++i) {
			aiString str;
			mat->GetTexture(type, i, &str);

			bool skip = false;

			for (GLuint j = 0; j < mTexturesLoaded.size(); ++j) {
				if (0 == std::strcmp(mTexturesLoaded[j].path.data(), str.C_Str())) {
					textures.push_back(mTexturesLoaded[j]);
					skip = true;
					break;
				}
			}

			if (!skip) {
				Texture texture;
				texture.id = TextureFromFile(str.C_Str(), this->directory);
				texture.type = typeName;
				texture.path = str.C_Str();
				textures.push_back(texture);
				mTexturesLoaded.push_back(texture);
			}
		}
		return textures;
	}
};

GLuint TextureFromFile(const char* path, const std::string& directory) {
	std::string filename = directory + "/" + path;

	GLuint textureID;
	glGenTextures(1, &textureID);

	stbi_set_flip_vertically_on_load(true);

	GLint width, height, nrComponents;
	unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (nullptr == data) {
		std::cout << "texture failed to load at path:" << path << std::endl;

	} else {
		GLenum format = GL_RGB;
		switch (nrComponents) {
			case 1:
				format = GL_RED;
				break;

			case 3:
				format = GL_RGB;
				break;

			case 4:
				format = GL_RGBA;
				break;

			default:
				break;
		}

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, static_cast<GLint>(format), width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	stbi_image_free(data);
	return textureID;
}


#endif
