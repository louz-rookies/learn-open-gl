#ifndef __SHADER_H__
#define __SHADER_H__

#include <glm/glm.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader {
public:
	GLuint program;

	Shader(const GLchar* vertexPath, const GLchar* fragmentPath) {
		std::string vertexCode;
		std::string fragmentCode;
		std::ifstream vShaderFile;
		std::ifstream fShaderFile;
		vShaderFile.exceptions(std::ifstream::failbit || std::ifstream::badbit);
		fShaderFile.exceptions(std::ifstream::failbit || std::ifstream::badbit);
		try {
			vShaderFile.open(vertexPath);
			std::stringstream vShaderStream;
			vShaderStream << vShaderFile.rdbuf();
			vShaderFile.close();
			vertexCode = vShaderStream.str();

			fShaderFile.open(fragmentPath);
			std::stringstream fShaderStream;
			fShaderStream << fShaderFile.rdbuf();
			vShaderFile.close();
			fragmentCode = fShaderStream.str();

		} catch (std::ifstream::failure& e) {
			std::cout << "error::shader::file_not_succesfully_read: " << e.what() << std::endl;
		}

		const GLchar* vShaderCode = vertexCode.c_str();
		const GLchar* fShaderCode = fragmentCode.c_str();

		GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, 1, &vShaderCode, nullptr);
		glCompileShader(vertexShader);
		checkCompileErrors(vertexShader, "VERTEX");

		GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, 1, &fShaderCode, nullptr);
		glCompileShader(fragmentShader);
		checkCompileErrors(fragmentShader, "FRAGMENT");

		program = glCreateProgram();
		glAttachShader(program, vertexShader);
		glAttachShader(program, fragmentShader);
		glLinkProgram(program);
		checkCompileErrors(fragmentShader, "PROGRAM");

		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
	}

	~Shader() {
		glDeleteProgram(program);
	}

	void use() {
		glUseProgram(program);
	}

	void setBool(const std::string& name, GLboolean value) const {
		glUniform1i(glGetUniformLocation(program, name.c_str()), static_cast<int>(value));
	}

	void setInt(const std::string& name, GLint value) const {
		glUniform1i(glGetUniformLocation(program, name.c_str()), value);
	}

	void setFloat(const std::string& name, GLfloat value) const {
		glUniform1f(glGetUniformLocation(program, name.c_str()), value);
	}

	void setVec2(const std::string& name, const glm::vec2& value) const {
		glUniform2fv(glGetUniformLocation(program, name.c_str()), 1, &value[0]);
	}

	void setVec2(const std::string& name, GLfloat x, GLfloat y) const {
		glUniform2f(glGetUniformLocation(program, name.c_str()), x, y);
	}

	void setVec3(const std::string& name, const glm::vec3& value) const {
		glUniform3fv(glGetUniformLocation(program, name.c_str()), 1, &value[0]);
	}

	void setVec3(const std::string& name, GLfloat x, GLfloat y, GLfloat z) const {
		glUniform3f(glGetUniformLocation(program, name.c_str()), x, y, z);
	}

	void setVec4(const std::string& name, const glm::vec4& value) const {
		glUniform4fv(glGetUniformLocation(program, name.c_str()), 1, &value[0]);
	}

	void setVec4(const std::string& name, GLfloat x, GLfloat y, GLfloat z, GLfloat w) const {
		glUniform4f(glGetUniformLocation(program, name.c_str()), x, y, z, w);
	}

	void setMat2(const std::string& name, const glm::mat2& mat) const {
		glUniformMatrix2fv(glGetUniformLocation(program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
	}

	void setMat3(const std::string& name, const glm::mat3& mat) const {
		glUniformMatrix3fv(glGetUniformLocation(program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
	}

	void setMat4(const std::string& name, const glm::mat4& mat) const {
		glUniformMatrix4fv(glGetUniformLocation(program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
	}

private:
	void checkCompileErrors(GLuint shader, std::string type) {
		GLint success;
		GLchar infoLog[1024] = { '\0' };
		if ("PROGRAM" != type) {
			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
			if (!success) {
				glGetShaderInfoLog(shader, 1024, nullptr, infoLog);
				std::cout << "error::shader_compilation_error of type:" << type << "\n" << infoLog << std::endl;
			}
		}
		else {
			glGetShaderiv(shader, GL_LINK_STATUS, &success);
			if (!success) {
				glGetShaderInfoLog(shader, 1024, nullptr, infoLog);
				std::cout << "error::program_linking_error of type:" << type << "\n" << infoLog << std::endl;
			}
		}
	}
};


#endif