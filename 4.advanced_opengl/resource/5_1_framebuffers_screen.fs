#version 330 core

out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D screenTexture;

void main() {
    FragColor = vec4(texture(screenTexture, TexCoords).rgb, 1.0f);

    // Inversion
    FragColor = vec4(vec3(1.0 - texture(screenTexture, TexCoords)), 1.0f);

    // Grayscale
    FragColor = texture(screenTexture, TexCoords);
    float average = (FragColor.r + FragColor.g + FragColor.b) / 3.0;
    FragColor = vec4(average, average, average, 1.0);

    // Grayscale + Weighted
    FragColor = texture(screenTexture, TexCoords);
    float average = 0.2126 * FragColor.r + 0.7152 * FragColor.g + 0.0722 * FragColor.b;
    FragColor = vec4(average, average, average, 1.0);

    const float offset = 1.0 / 300.0;

    vec2 offsets[9] = vec2[](
        vec2(-offset,  offset), // left_top
        vec2( 0.0f,    offset), // mid_top
        vec2( offset,  offset), // right_top
        vec2(-offset,  0.0f),   // left
        vec2( 0.0f,    0.0f),   // mid
        vec2( offset,  0.0f),   // right
        vec2(-offset, -offset), // left_bottom
        vec2( 0.0f,   -offset), // mid_bottom
        vec2( offset, -offset)  // right_bottom
    );

    vec3 sampleTex[9];
    for(int i = 0; i < 9; i++) {
        sampleTex[i] = vec3(texture(screenTexture, TexCoords.st + offsets[i]));
    }

    // Sharpen
    float sharpen_kernel[9] = float[](
        -1, -1, -1,
        -1,  9, -1,
        -1, -1, -1
    );
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++) {
        col += sampleTex[i] * sharpen_kernel[i];
    }

    // Blur
    float blur_kernel[9] = float[](
        1.0/16, 2.0/16, 1.0/16,
        2.0/16, 4.0/16, 2.0/16,
        1.0/16, 2.0/16, 1.0/16
    );

    col = vec3(0.0);
    for(int i = 0; i < 9; i++) {
        col += sampleTex[i] * blur_kernel[i];
    }

    // Edge-detection
    float edge_detection_kernel[9] = float[] (
        1, 1, 1,
        1, -8, 1,
        1, 1, 1
    );

    col = vec3(0.0);
    for(int i = 0; i < 9; i++) {
        col += sampleTex[i] * edge_detection_kernel[i];
    }

    FragColor = vec4(col, 1.0);
}