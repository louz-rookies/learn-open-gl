#include "game_object.h"

GameObject::GameObject():
	mPos(0.0f, 0.0f), mSize(1.0f, 1.0f), mVelocity(0.0f), mColor(1.0f), mColorLevel(0), mRotation(0.0f), mSprite(), mIsSolid(false), mDestroyed(false) {
}

GameObject::~GameObject() {
}


GameObject::GameObject(glm::vec2 pos, glm::vec2 size, Texture2D sprite, glm::vec3 color, GLuint colorLvl, glm::vec2 velocity):
	mPos(pos), mSize(size), mVelocity(velocity), mColor(color), mColorLevel(colorLvl), mRotation(0.0f), mSprite(sprite), mIsSolid(false), mDestroyed(false) {
}

void GameObject::Draw(SpriteRenderer& renderer) {
	renderer.DrawSprite(mSprite, mPos, mSize, mRotation, mColor);
}