#ifndef __2DGAME_TEXTURE_H__
#define __2DGAME_TEXTURE_H__

#include <glad/glad.h>

class Texture2D {
public:
	Texture2D();
	~Texture2D();

	void Generate(GLuint width, GLuint height, unsigned char* data);
	void Bind() const;

	void SetInternalFormat(GLuint format);
	void SetImageFormat(GLuint format);

	GLuint GetTexture2dID() {
		return mID;
	}

private:
	GLuint mID;
	GLuint mWidth, mHeight;
	GLuint mInternalFormat, mImageFormat;
	GLuint mWrapS, mWrapT;
	GLuint mFilterMin, mFilterMax;
};

#endif