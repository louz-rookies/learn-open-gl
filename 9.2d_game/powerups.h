#ifndef __2DGAME_POWERUPS_H__
#define __2DGAME_POWERUPS_H__

#include <string>

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "game_object.h"

const glm::vec2 POWERUP_SIZE(60.0f, 20.0f);
const glm::vec2 POWERUP_VELOCITY(0.0f, 150.0f);
const GLuint POWERUP_COLORLEVEL = 1;

class PowerUp : public GameObject {
public:
	PowerUp(std::string type, glm::vec3 color, GLfloat duration, glm::vec2 position, Texture2D texture):
		GameObject(position, POWERUP_SIZE, texture, color, POWERUP_COLORLEVEL, POWERUP_VELOCITY), mType(type), mDuration(duration), mActivated() {}

	std::string GetType() const {
		return mType;
	}

	GLfloat GetDuration() const {
		return mDuration;
	}

	void SetDuration(GLfloat duration) {
		mDuration = duration;
	}

	GLboolean GetActivated() const {
		return mActivated;
	}

	void SetActivated(GLboolean actived) {
		mActivated = actived;
	}

private:
	std::string mType;
	GLfloat mDuration;
	GLboolean mActivated;
};

#endif