#ifndef __2DGAME_GAME_LEVEL_H__
#define __2DGAME_GAME_LEVEL_H__

#include <vector>

#include "game_object.h"
#include "resource_manager.h"

const GLint TILE_ATTR_SOLID = 1;

class GameLevel {
public:
	GameLevel() {}
	void Load(const GLchar* file, GLuint levelWidth, GLuint levelHeight);
	void Draw(SpriteRenderer& renderer);
	GLboolean IsCompleted();

	std::vector<GameObject>& GetBricks() {
		return mBricks;
	}

private:
	void Init(std::vector<std::vector<GLuint> > tileData, GLuint levelWidth, GLuint levelHeight);
	void UpdateBricks();

private:
	std::vector<GameObject> mBricks;
};

#endif