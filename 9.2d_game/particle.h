#ifndef __2DGAME_PARTICLE_H__
#define __2DGAME_PARTICLE_H__

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <vector>

#include "texture.h"
#include "shader.h"
#include "game_object.h"

class Particle {
public:
	Particle() : mPos(0.0f), mVelocity(0.0f), mColor(0.0f), mLife(0.0f) {}

	GLfloat GetLife() const {
		return mLife;
	}

	void SetLife(GLfloat life) {
		mLife = life;
	}

	glm::vec2 GetPos() const {
		return mPos;
	}

	void SetPos(glm::vec2 pos) {
		mPos = pos;
	}

	glm::vec2 GetVelocity() const {
		return mVelocity;
	}

	void SetVelocity(glm::vec2 vel) {
		mVelocity = vel;
	}

	glm::vec4 GetColor() const {
		return mColor;
	}

	void SetColor(glm::vec4 color) {
		mColor = color;
	}

	void SetColorA(GLfloat alpha) {
		mColor.a = alpha;
	}

private:
	glm::vec2 mPos;
	glm::vec2 mVelocity;
	glm::vec4 mColor;
	GLfloat mLife;
};


class ParticleGenerator {
public:
	ParticleGenerator(Shader shader, Texture2D texture, GLuint amount);
	void Update(GLfloat dt, GameObject& object, GLuint newParticles, glm::vec2 offset = glm::vec2(0.0f, 0.0f));
	void Draw();

private:
	void Init();
	GLuint firstUnusedParticle();
	void RespawnParticle(Particle& particle, GameObject& object, glm::vec2 offset = glm::vec2(0.0f, 0.0f));

private:
	Shader mShader;
	Texture2D mTexture;
	GLuint mAmount;

	std::vector<Particle> mParticles;
	GLuint mVAO;

	GLuint mLastUsedParticle;
};

#endif