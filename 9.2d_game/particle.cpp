#include "particle.h"

#include <iostream>

ParticleGenerator::ParticleGenerator(Shader shader, Texture2D texture, GLuint amount):
    mShader(shader), mTexture(texture), mAmount(amount) {
	Init();
}

void ParticleGenerator::Init() {
	GLfloat particleQuad[] = {
        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f,

        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f
	};
    GLuint VBO;

    glGenVertexArrays(1, &mVAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(mVAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(particleQuad), particleQuad, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    for (GLuint i = 0; i < mAmount; ++i) {
        mParticles.push_back(Particle());
    }
}

void ParticleGenerator::Draw() {
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    mShader.Use();
    for (const Particle& particle : mParticles) {
        if (particle.GetLife() > 0.0f) {
            mShader.SetVector2f("offset", particle.GetPos());
            mShader.SetVector4f("color", particle.GetColor());
            mTexture.Bind();
            glBindVertexArray(mVAO);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            glBindVertexArray(0);
        }
    }
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void ParticleGenerator::Update(GLfloat dt, GameObject& object, GLuint newParticles, glm::vec2 offset) {
    for (GLuint i = 0; i < newParticles; ++i) {
        GLuint unusedParticle = firstUnusedParticle();
        RespawnParticle(mParticles[unusedParticle], object, offset);
    }

    for (GLuint i = 0; i < mAmount; ++i) {
        Particle& p = mParticles[i];
        p.SetLife(p.GetLife() - dt);
        if (p.GetLife() > 0.0f) {
            p.SetPos(p.GetPos() - p.GetVelocity() * dt);
            p.SetColorA(p.GetColor().a - 2.5f * dt);
        }
    }
}

GLuint ParticleGenerator::firstUnusedParticle() {
    
    for (GLuint i = mLastUsedParticle; i < mAmount; ++i) {
        if (mParticles[i].GetLife() <= 0.0f) {
            mLastUsedParticle = i;
            return i;
        }
    }

    for (GLuint i = 0; i < mLastUsedParticle; ++i) {
        if (mParticles[i].GetLife() <= 0.0f) {
            mLastUsedParticle = i;
            return i;
        }
    }

    mLastUsedParticle = 0;
    return 0;
}

void ParticleGenerator::RespawnParticle(Particle& particle, GameObject& object, glm::vec2 offset) {
    GLfloat random = ((rand() % 100) - 50) / 10.0f;
    GLfloat rColor = 0.5f + ((rand() % 100) / 100.f);
    particle.SetPos(object.GetPos() + random + offset);
    particle.SetColor(glm::vec4(rColor, rColor, rColor, 1.0f));
    particle.SetLife(1.0f);
    particle.SetVelocity(object.GetVelocity() * 0.1f);
}