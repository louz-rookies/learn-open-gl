#include "resource_manager.h"

#include <iostream>
#include <sstream>
#include <fstream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

std::map<std::string, Shader> ResourceManager::sShaders;
std::map<std::string, Texture2D> ResourceManager::sTextures;

ResourceManager::ResourceManager() {
}


Shader ResourceManager::LoadShader(const GLchar* vShaderFile, const GLchar* fShaderFile, std::string name) {
	sShaders[name] = LoadShaderFromFile(vShaderFile, fShaderFile);
	return sShaders[name];
}

Shader ResourceManager::GetShader(std::string name) {
	return sShaders[name];
}

Shader ResourceManager::LoadShaderFromFile(const GLchar* vShaderFile, const GLchar* fShaderFile) {
	std::string vertexCode;
	std::string fragmentCode;
	try {
		std::stringstream vShaderStream;
		std::ifstream vertexShaderFile(vShaderFile);
		vShaderStream << vertexShaderFile.rdbuf();
		vertexShaderFile.close();
		vertexCode = vShaderStream.str();

		std::stringstream fShaderStream;
		std::ifstream fragmentShaderFile(fShaderFile);
		fShaderStream << fragmentShaderFile.rdbuf();
		fragmentShaderFile.close();
		fragmentCode = fShaderStream.str();
	} catch (std::exception e) {
		std::cout << "error::shader::failed to load shader files." << std::endl;
	}

	const GLchar* vShaderCode = vertexCode.c_str();
	const GLchar* fShaderCode = fragmentCode.c_str();

	Shader shader;
	shader.Compile(vShaderCode, fShaderCode);
	return shader;
}

Texture2D ResourceManager::LoadTexture(const GLchar* file, bool alpha, std::string name) {
	sTextures[name] = LoadTextureFromFile(file, alpha);
	return sTextures[name];
}

Texture2D ResourceManager::GetTexture(std::string name) {
	return sTextures[name];
}

void ResourceManager::Clear() {
	for (std::map<std::string, Shader>::iterator iter = sShaders.begin(); iter != sShaders.end(); ++iter) {
		glDeleteShader(iter->second.GetShaderID());
	}

	for (std::map<std::string, Texture2D>::iterator iter = sTextures.begin(); iter != sTextures.end(); ++iter) {
		GLuint id = iter->second.GetTexture2dID();
		glDeleteTextures(1, &id);
	}
}

Texture2D ResourceManager::LoadTextureFromFile(const GLchar* file, bool alpha) {
	Texture2D texture;
	if (alpha) {
		texture.SetInternalFormat(GL_RGBA);
		texture.SetImageFormat(GL_RGBA);
	}

	//stbi_set_flip_vertically_on_load(true);
	GLint width, height, nrChannels;
	unsigned char* data = stbi_load(file, &width, &height, &nrChannels, 0);
	if (data == nullptr) {
		std::cout << "error::texture::failed to load texture files." << std::endl;
	} else {
		texture.Generate(width, height, data);
		stbi_image_free(data);
	}

	return texture;
}