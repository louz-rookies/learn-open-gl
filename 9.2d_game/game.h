#ifndef __2DGAME_GAME_H__
#define __2DGAME_GAME_H__

#include <tuple>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "game_level.h"
#include "ball_object.h"
#include "powerups.h"

enum class E_GAME_STATE {
	GAME_ACTIVE,
	GAME_MENU,
	GAME_WIN
};

enum class E_DIRECTION {
	DIR_UP,
	DIR_RIGHT,
	DIR_DOWN,
	DIR_LEFT
};

typedef std::tuple<GLboolean, E_DIRECTION, glm::vec2> Collision;

const glm::vec2 PLAYER_SIZE(100.0f, 20.0f);
const GLfloat PLAYER_VELOCITY(500.0f);

const glm::vec2 BALL_INIT_VELOCITY(100.0f, -350.0f);
const GLfloat BALL_RADIUS = 12.5f;

const GLint PARTICLE_AMOUNT = 500;

const GLuint RANDOM1_SPAWN = 75;
const GLuint RANDOM2_SPAWN = 15;

const glm::vec3 COLOR_STICKY = glm::vec3(1.0f, 0.5f, 1.0f);
const glm::vec3 COLOR_PASSTHROUGH = glm::vec3(1.0f, 0.5f, 0.5f);

class Game {
public:
	Game(GLuint width, GLuint height);
	~Game();

	void Init();
	void ProcessInput(GLfloat dt);
	void Update(GLfloat dt);
	void Render();
	void DoCollisions();

	void SetKeys(GLuint index, GLboolean state);
	void SetKeysPressed(GLuint index, GLboolean state);

	void SpawnPowerUps(GameObject& block);
	void UpdatePowerUps(GLfloat dt);

private:
	GLboolean CheckCollision(const GameObject& first, const GameObject& second);
	Collision CheckCollision(const BallObject& first, const GameObject& second);
	E_DIRECTION VectorDirection(glm::vec2 target);

	void ResetLevel();
	void ResetPlayer();

	GLboolean ShouldSpawn(GLuint chance);
	void ActivatePowerUp(PowerUp& powerUp);
	GLboolean IsOtherPowerUpActive(std::vector<PowerUp>& powerUps, std::string type);

private:
	E_GAME_STATE mState;
	GLboolean mKeys[1024];
	GLboolean mKeysPressed[1024];
	GLuint mWidth;
	GLuint mHeight;
	std::vector<GameLevel> mLevels;
	GLuint mLevel;
	GLuint mLives;
	std::vector<PowerUp> mPowerUps;
};

#endif