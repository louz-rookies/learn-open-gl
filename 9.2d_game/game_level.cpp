#include "game_level.h"

#include <fstream>
#include <sstream>

void GameLevel::Load(const GLchar* file, GLuint levelWidth, GLuint levelHeight) {
	mBricks.clear();

	std::ifstream fstream(file);
	std::string line;
	GLuint tileCode;
	std::vector<std::vector<GLuint>> tileData;
	if (fstream) {
		while (std::getline(fstream, line)) {
			std::istringstream sstream(line);
			std::vector<GLuint> row;
			while (sstream >> tileCode) {
				row.push_back(tileCode);
			}
			tileData.push_back(row);
		}
		if (tileData.size() > 0) {
			Init(tileData, levelWidth, levelHeight);
		}
	}
}

void GameLevel::Draw(SpriteRenderer& renderer) {
	UpdateBricks();

	for (GameObject& tile : mBricks) {
		if (!tile.GetDestroyed()) {
			tile.Draw(renderer);
		}
	}
}

GLboolean GameLevel::IsCompleted() {
	for (const GameObject& tile : mBricks) {
		if (!tile.GetIsSolid() && !tile.GetDestroyed()) {
			return false;
		}
	}
	return true;
}

void GameLevel::Init(std::vector<std::vector<GLuint> > tileData, GLuint levelWidth, GLuint levelHeight) {
	GLuint height = static_cast<GLuint>(tileData.size());
	GLuint width = static_cast<GLuint>(tileData[0].size());
	GLfloat unitWidth = levelWidth / static_cast<GLfloat>(width);
	GLfloat unitHight = levelHeight / static_cast<GLfloat>(height);

	for (GLuint y = 0; y < height; ++y) {
		for (GLuint x = 0; x < width; ++x) {
			if (TILE_ATTR_SOLID == tileData[y][x]) {
				glm::vec2 pos(unitWidth * x, unitHight * y);
				glm::vec2 size(unitWidth, unitHight);
				GameObject gameObj(pos, size, ResourceManager::GetTexture("block_solid"), COLOR_LV1, 1);
				gameObj.SetIsSolid(true);
				mBricks.push_back(gameObj);
			}
			else if (tileData[y][x] > TILE_ATTR_SOLID) {
				glm::vec3 color = glm::vec3(1.0f);
				GLuint colorLevel = 0;
				switch (tileData[y][x]) {
				case 2:
					color = COLOR_LV2;
					colorLevel = 2;
					break;

				case 3:
					color = COLOR_LV3;
					colorLevel = 3;
					break;

				case 4:
					color = COLOR_LV4;
					colorLevel = 4;
					break;

				case 5:
					color = COLOR_LV5;
					colorLevel = 5;
					break;

				default:
					break;
				}
				glm::vec2 pos(unitWidth * x, unitHight * y);
				glm::vec2 size(unitWidth, unitHight);
				mBricks.push_back(GameObject(pos, size, ResourceManager::GetTexture("block"), color, colorLevel));
			}
		}
	}
}

void GameLevel::UpdateBricks() {
	for (GameObject& tile : mBricks) {
		switch (tile.GetColorLevel()) {
		case 2:
			tile.SetColor(COLOR_LV2);
			break;

		case 3:
			tile.SetColor(COLOR_LV3);
			break;

		case 4:
			tile.SetColor(COLOR_LV4);
			break;

		case 5:
			tile.SetColor(COLOR_LV5);
			break;

		default:
			break;
		}

	}
}