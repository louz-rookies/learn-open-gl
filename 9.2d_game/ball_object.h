#ifndef __2DGAME_BALL_OBJECT_H__
#define __2DGAME_BALL_OBJECT_H__

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "game_object.h"
#include "texture.h"

class BallObject : public GameObject {
public:
	BallObject();
	BallObject(glm::vec2 pos, GLfloat radius, glm::vec2 velocity, Texture2D sprite);
	glm::vec2 Move(GLfloat dt, GLuint winWidth);
	void Reset(glm::vec2 pos, glm::vec2 vel);

	void SetRadius(GLfloat radius) {
		mRadius = radius;
	}

	GLfloat GetRadius() const {
		return mRadius;
	}

	void SetStuck(GLboolean stuck) {
		mStuck = stuck;
	}

	GLboolean GetStuck() const {
		return mStuck;
	}

	void SetSticky(GLboolean sticky) {
		mSticky = sticky;
	}

	GLboolean GetSticky() const {
		return mSticky;
	}

	void SetPassThrough(GLboolean passthrough) {
		mPassThrough = passthrough;
	}

	GLboolean GetPassThrough() const {
		return mPassThrough;
	}

private:
	GLfloat mRadius;
	GLboolean mStuck;
	GLboolean mSticky;
	GLboolean mPassThrough;
};


#endif