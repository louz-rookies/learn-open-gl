#include "texture.h"

#include <iostream>

Texture2D::Texture2D() :
	mID(0), mWidth(0), mHeight(0), mInternalFormat(GL_RGB), mImageFormat(GL_RGB),
	mWrapS(GL_REPEAT), mWrapT(GL_REPEAT), mFilterMin(GL_LINEAR), mFilterMax(GL_LINEAR) {
	glGenTextures(1, &mID);
}

Texture2D::~Texture2D() {
	mID = 0;
	mWidth = 0;
	mHeight = 0;
	mInternalFormat = GL_RGB;
	mImageFormat = GL_RGB;
	mWrapS = GL_REPEAT;
	mWrapT = GL_REPEAT;
	mFilterMin = GL_LINEAR;
	mFilterMax = GL_LINEAR;
}

void Texture2D::Generate(GLuint width, GLuint height, unsigned char* data) {
	mWidth = width;
	mHeight = height;
	glBindTexture(GL_TEXTURE_2D, mID);
	glTexImage2D(GL_TEXTURE_2D, 0, mInternalFormat, width, height, 0, mImageFormat, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mWrapS);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mWrapT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mFilterMin);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mFilterMax);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture2D::Bind() const {
	glBindTexture(GL_TEXTURE_2D, mID);
}

void Texture2D::SetInternalFormat(GLuint format) {
	mInternalFormat = format;
}

void Texture2D::SetImageFormat(GLuint format) {
	mImageFormat = format;
}