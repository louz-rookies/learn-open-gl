#ifndef __2DGAME_GAME_OBJECT_H__
#define __2DGAME_GAME_OBJECT_H__

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "texture.h"
#include "sprite_renderer.h"

const glm::vec3 COLOR_LV1 =  glm::vec3(0.8f, 0.8f, 0.7f);
const glm::vec3 COLOR_LV2 =  glm::vec3(0.2f, 0.6f, 1.0f);
const glm::vec3 COLOR_LV3 =  glm::vec3(0.0f, 0.7f, 0.0f);
const glm::vec3 COLOR_LV4 =  glm::vec3(0.8f, 0.8f, 0.4f);
const glm::vec3 COLOR_LV5 =  glm::vec3(1.0f, 0.5f, 0.0f);

class GameObject {
public:
	GameObject();
	GameObject(glm::vec2 pos, glm::vec2 size, Texture2D sprite, glm::vec3 color = glm::vec3(1.0f), GLuint colorLvl = 0, glm::vec2 velocity = glm::vec2(0.0f, 0.0f));
	~GameObject();
	virtual void Draw(SpriteRenderer& renderer);


	void SetPos(glm::vec2 pos) {
		mPos = pos;
	}

	void SetPosX(GLfloat x) {
		mPos.x = x;
	}

	void SetPosY(GLfloat y) {
		mPos.y = y;
	}

	glm::vec2 GetPos() const {
		return mPos;
	}

	void SetSize(glm::vec2 size) {
		mSize = size;
	}

	glm::vec2 GetSize() const {
		return mSize;
	}

	void SetVelocity(glm::vec2 vel) {
		mVelocity = vel;
	}

	void SetVelocityX(GLfloat vx) {
		mVelocity.x = vx;
	}

	void SetVelocityY(GLfloat vy) {
		mVelocity.y = vy;
	}

	glm::vec2 GetVelocity() const {
		return mVelocity;
	}

	void SetColor(glm::vec3 color) {
		mColor = color;
	}

	glm::vec3 GetColor() const {
		return mColor;
	}

	void SetRotation(GLfloat rotate) {
		mRotation = rotate;
	}

	GLfloat GetRotation() const {
		return mRotation;
	}

	void SetIsSolid(GLboolean isSolid) {
		mIsSolid = isSolid;
	}

	GLboolean GetIsSolid() const {
		return mIsSolid;
	}

	void SetDestroyed(GLboolean destroyed) {
		mDestroyed = destroyed;
	}

	GLboolean GetDestroyed() const {
		return mDestroyed;
	}

	GLuint GetColorLevel() const {
		return mColorLevel;
	}

	void SetColorLevel(int ColorLvl) {
		mColorLevel = ColorLvl;
	}

protected:
	glm::vec2 mPos;
	glm::vec2 mSize;
	glm::vec2 mVelocity;
	glm::vec3 mColor;
	GLfloat mRotation;
	GLboolean mIsSolid;
	GLboolean mDestroyed;
	Texture2D mSprite;
	GLuint mColorLevel;
};

#endif

