#ifndef __2DGAME_SHADER_H__
#define __2DGAME_SHADER_H__

#include <string>

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader {
public:
	Shader();
	~Shader();

	Shader& Use();
	void Compile(const GLchar* vertexSource, const GLchar* fragmentSource);

	void SetFloat(const GLchar* name, GLfloat value, bool useShader = false);
	void SetInteger(const GLchar* name, int value, bool useShader = false);
	void SetVector2f(const GLchar* name, GLfloat x, GLfloat y, bool useShader = false);
	void SetVector2f(const GLchar* name, const glm::vec2& value, bool useShader = false);
	void SetVector3f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, bool useShader = false);
	void SetVector3f(const GLchar* name, const glm::vec3& value, bool useShader = false);
	void SetVector4f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, bool useShader = false);
	void SetVector4f(const GLchar* name, const glm::vec4& value, bool useShader = false);
	void SetMatrix4(const GLchar* name, const glm::mat4& matrix, bool useShader = false);

	GLuint GetShaderID() const {
		return mID;
	}

private:
	void CheckCompileErrors(GLuint object, std::string type);

private:
	GLuint mID;
};


#endif