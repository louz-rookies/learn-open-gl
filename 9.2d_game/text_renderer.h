#ifndef __2DGAME_TEXT_RENDERER_H__
#define __2DGAME_TEXT_RENDERER_H__

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

#include <map>

#include "shader.h"
#include "texture.h"

struct Character {
	GLuint textureID;
	glm::ivec2 size;
	glm::ivec2 bearing;
	GLuint advance;
};

class TextRenderer {
public:
	TextRenderer(GLuint width, GLuint height);

	void Load(std::string font, GLuint fontSize);
	void RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color = glm::vec3(1.0f));

private:
	Shader mShader;
	GLuint mVAO;
	GLuint mVBO;
	std::map<GLchar, Character> mCharacters;
};



#endif