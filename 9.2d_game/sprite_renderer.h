#ifndef __2DGAME_SPRITE_RENDERER_H__
#define __2DGAME_SPRITE_RENDERER_H__

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "texture.h"
#include "shader.h"

class SpriteRenderer {
public:
	SpriteRenderer(const Shader& shader);
	~SpriteRenderer();
	void DrawSprite(const Texture2D& texture, glm::vec2 pos, glm::vec2 size = glm::vec2(10.0f, 10.0f), GLfloat rotate = 0.0f, glm::vec3 color = glm::vec3(1.0f));

private:
	void InitRenderData();

private:
	Shader mShader;
	GLuint mQuadVAO;
};


#endif