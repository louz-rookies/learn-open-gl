#include "ball_object.h"

BallObject::BallObject() :
	GameObject(),  mRadius(12.5f), mStuck(true) {
}

BallObject::BallObject(glm::vec2 pos, GLfloat radius, glm::vec2 velocity, Texture2D sprite) : 
	GameObject(pos, glm::vec2(radius * 2.0f, radius * 2.0f), sprite, glm::vec3(1.0f), 1, velocity), 
	mRadius(radius), mStuck(GL_TRUE), mSticky(GL_FALSE), mPassThrough(GL_FALSE) {
}

glm::vec2 BallObject::Move(GLfloat dt, GLuint winWidth) {
	if (!mStuck) {
		mPos += mVelocity * dt;

		if (mPos.x <= 0.0f) {
			mVelocity.x = -mVelocity.x;
			mPos.x = 0.0f;
		}
		else if (mPos.x + mSize.x >= winWidth) {
			mVelocity.x = -mVelocity.x;
			mPos.x = winWidth - mSize.x;
		}
		else if (mPos.y <= 0.0f) {
			mVelocity.y = -mVelocity.y;
			mPos.y = 0.0f;
		}
	}

	return mPos;
}

void BallObject::Reset(glm::vec2 pos, glm::vec2 vel) {
	mPos = pos;
	mVelocity = vel;
	mStuck = true;
}