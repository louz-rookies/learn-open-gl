#include "post_processor.h"

#include <iostream>

PostProcessor::PostProcessor(Shader shader, GLuint width, GLuint height) :
	mShader(shader), mWidth(width), mHeight(height), mConfuse(GL_FALSE), mChaos(GL_FALSE), mShake(GL_FALSE){

	glGenFramebuffers(1, &mMSAAFBO);
	glGenRenderbuffers(1, &mRBO);
	glBindFramebuffer(GL_FRAMEBUFFER, mMSAAFBO);
	glBindRenderbuffer(GL_RENDERBUFFER, mRBO);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_RGB, mWidth, mHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mRBO);
	if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER)) {
		std::cout << "error::post processor::failed to init MSAA FBO" << std::endl;
	}
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glGenFramebuffers(1, &mFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, mFBO);
	mTexture.Generate(mWidth, mHeight, nullptr);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mTexture.GetTexture2dID(), 0);
	if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER)) {
		std::cout << "error::post processor::failed to init FBO" << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	InitRenderData();
	mShader.SetInteger("scene", 0, GL_TRUE);

	GLfloat offset = 1.0f / 300.0f;
	GLfloat offsets[9][2] = { // T:top C:center B:buttom L:left R:right
		{ -offset, offset  }, // TL
		{    0.0f, offset  }, // TC
		{  offset, offset  }, // TR
		{ -offset, 0.0f    }, // CL
		{    0.0f, 0.0f    }, // CC
		{  offset, 0.0f    }, // CR
		{ -offset, -offset }, // BL
		{    0.0f, -offset }, // BC
		{  offset, -offset }, // BR
	};
	glUniform2fv(glGetUniformLocation(mShader.GetShaderID(), "offsets"), 9, (GLfloat*)offsets);

	GLint edgeKernel[9] = {
	    -1, -1, -1,
		-1,  8, -1,
		-1, -1, -1
	};
	glUniform1iv(glGetUniformLocation(mShader.GetShaderID(), "edgeKernel"), 9, edgeKernel);

	GLfloat blurKernel[9] = {
		1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f,
		2.0f / 16.0f, 4.0f / 16.0f, 2.0f / 16.0f,
		1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f
	};
	glUniform1fv(glGetUniformLocation(mShader.GetShaderID(), "blurKernel"), 9, blurKernel);
}

void PostProcessor::InitRenderData() {
	GLuint VBO;
	GLfloat vertices[] = {
		// pos        // tex
		-1.0f, -1.0f, 0.0f, 0.0f,
		 1.0f,  1.0f, 1.0f, 1.0f,
		-1.0f,  1.0f, 0.0f, 1.0f,

		-1.0f, -1.0f, 0.0f, 0.0f,
		 1.0f, -1.0f, 1.0f, 0.0f,
		 1.0f,  1.0f, 1.0f, 1.0f
	};
	glGenVertexArrays(1, &mVAO);
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindVertexArray(mVAO);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void PostProcessor::BeginRender() {
	glBindFramebuffer(GL_FRAMEBUFFER, mMSAAFBO);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
}

void PostProcessor::EndRender() {
	glBindFramebuffer(GL_READ_FRAMEBUFFER, mMSAAFBO);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, mFBO);
	glBlitFramebuffer(0, 0, mWidth, mHeight, 0, 0, mWidth, mHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PostProcessor::Render(GLfloat time) {
	mShader.Use();
	mShader.SetFloat("time", time);
	mShader.SetInteger("confuse", mConfuse);
	mShader.SetInteger("chaos", mChaos);
	mShader.SetInteger("shake", mShake);

	glActiveTexture(GL_TEXTURE0);
	mTexture.Bind();
	glBindVertexArray(mVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}