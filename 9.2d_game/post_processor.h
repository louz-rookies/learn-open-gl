#ifndef __2DGAME_POST_PROCESSOR_H__
#define __2DGAME_POST_PROCESSOR_H__

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "shader.h"
#include "texture.h"

class PostProcessor {
public:
	PostProcessor(Shader shader, GLuint width, GLuint height);

	void BeginRender();
	void EndRender();
	void Render(GLfloat time); 

	GLboolean GetShaker() const {
		return mShake;
	}

	void SetShaker(GLboolean shake) {
		mShake = shake;
	}

	GLboolean GetConfuse() const {
		return mConfuse;
	}

	void SetConfuse(GLboolean confuse) {
		mConfuse = confuse;
	}

	GLboolean GetChaos() const {
		return mChaos;
	}

	void SetChaos(GLboolean chaos) {
		mChaos = chaos;
	}

private:
	void InitRenderData();

private:
	GLuint mWidth;
	GLuint mHeight;
	Shader mShader;
	GLuint mVAO;
	GLuint mMSAAFBO;
	GLuint mRBO;
	Texture2D mTexture;
	GLuint mFBO;
	GLboolean mConfuse, mChaos, mShake;
};


#endif 