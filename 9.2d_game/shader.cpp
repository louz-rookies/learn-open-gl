#include "shader.h"

#include <iostream>

Shader::Shader() : mID(0) {
}

Shader::~Shader() {
}

Shader& Shader::Use() {
	glUseProgram(mID);
	return *this;
}

void Shader::Compile(const GLchar* vertexSource, const GLchar* fragmentSource) {
	GLuint sVertex, sFragment;

	sVertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(sVertex, 1, &vertexSource, nullptr);
	glCompileShader(sVertex);
	CheckCompileErrors(sVertex, "VERTEX");

	sFragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(sFragment, 1, &fragmentSource, nullptr);
	glCompileShader(sFragment);
	CheckCompileErrors(sFragment, "FRAGMENT");

	mID = glCreateProgram();
	glAttachShader(mID, sVertex);
	glAttachShader(mID, sFragment);
	glLinkProgram(mID);
	CheckCompileErrors(mID, "PROGRAM");

	glDeleteShader(sVertex);
	glDeleteShader(sFragment);
}

void Shader::SetFloat(const GLchar* name, GLfloat value, bool useShader) {
	if (useShader) Use();
	glUniform1f(glGetUniformLocation(mID, name), value);
}

void Shader::SetInteger(const GLchar* name, int value, bool useShader) {
	if (useShader) Use();
	glUniform1i(glGetUniformLocation(mID, name), value);
}

void Shader::SetVector2f(const GLchar* name, GLfloat x, GLfloat y, bool useShader) {
	if (useShader) Use();
	glUniform2f(glGetUniformLocation(mID, name), x, y);
}

void Shader::SetVector2f(const GLchar* name, const glm::vec2& value, bool useShader) {
	if (useShader) Use();
	glUniform2f(glGetUniformLocation(mID, name), value.x, value.y);
}

void Shader::SetVector3f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, bool useShader) {
	if (useShader) Use();
	glUniform3f(glGetUniformLocation(mID, name), x, y, z);
}

void Shader::SetVector3f(const GLchar* name, const glm::vec3& value, bool useShader) {
	if (useShader) Use();
	glUniform3f(glGetUniformLocation(mID, name), value.x, value.y, value.z);
}

void Shader::SetVector4f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, bool useShader) {
	if (useShader) Use();
	glUniform4f(glGetUniformLocation(mID, name), x, y, z, w);
}

void Shader::SetVector4f(const GLchar* name, const glm::vec4& value, bool useShader) {
	if (useShader) Use();
	glUniform4f(glGetUniformLocation(mID, name), value.x, value.y, value.z, value.w);
}

void Shader::SetMatrix4(const GLchar* name, const glm::mat4& matrix, bool useShader) {
	if (useShader) Use();
	//glUniformMatrix4fv(glGetUniformLocation(mID, name), 1, GL_FALSE, glm::value_ptr(matrix)); // glm::value_ptr(matrix) // &matrix[0][0]
	glUniformMatrix4fv(glGetUniformLocation(mID, name), 1, GL_FALSE, glm::value_ptr(matrix)); // glm::value_ptr(matrix) // &matrix[0][0]
}

void Shader::CheckCompileErrors(GLuint object, std::string type) {
	GLint success;
	GLchar infoLog[1024];

	if ("PROGRAM" != type) {
		glGetShaderiv(object, GL_COMPILE_STATUS, &success);
		if (!success) {
			glGetShaderInfoLog(object, 1024, nullptr, infoLog);
			std::cout << " error::shader::compile_time error::type " << type << std::endl;
		}
	}
	else {
		glGetProgramiv(object, GL_LINK_STATUS, &success);
		if (!success) {
			glGetProgramInfoLog(object, 1024, nullptr, infoLog);
			std::cout << " error::shader::link_time error::type " << type << std::endl;
		}
	}
}