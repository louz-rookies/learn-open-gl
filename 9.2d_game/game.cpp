#include <iostream>
#include <sstream>

#include <irrKlang/irrKlang.h>
#pragma comment (lib, "irrKlang.lib") // don't ignore

#include "game.h"
#include "resource_manager.h"
#include "sprite_renderer.h"
#include "game_object.h"
#include "ball_object.h"
#include "particle.h"
#include "post_processor.h"
#include "text_renderer.h"

GLfloat ShakeTime = 0.0f;

SpriteRenderer* pSpriteRenderer;
GameObject* pPlayer;
BallObject* pBall;
ParticleGenerator* pParticles;
PostProcessor* pPostProcessor;
irrklang::ISoundEngine* pSoundEngine = irrklang::createIrrKlangDevice();
TextRenderer* pTextRenderer;

Game::Game(GLuint width, GLuint height) :
	mState(E_GAME_STATE::GAME_MENU), mKeys(), mWidth(width), mHeight(height), mLives(3) {
}

Game::~Game() {
	if (nullptr != pSpriteRenderer) {
		delete pSpriteRenderer;
		pSpriteRenderer = nullptr;
	}

	if (nullptr != pPlayer) {
		delete pPlayer;
		pPlayer = nullptr;
	}

	if (nullptr != pBall) {
		delete pBall;
		pBall = nullptr;
	}

	if (nullptr != pParticles) {
		delete pParticles;
		pParticles = nullptr;
	}

	if (nullptr != pPostProcessor) {
		delete pPostProcessor;
		pPostProcessor = nullptr;
	}

	if (nullptr != pSoundEngine) {
		pSoundEngine->drop();
	}

	if (nullptr != pTextRenderer) {
		delete pTextRenderer;
		pTextRenderer = nullptr;
	}
}

void Game::Init() {
	ResourceManager::LoadShader("shaders/sprite.vs", "shaders/sprite.fs", "sprite");
	ResourceManager::LoadShader("shaders/particle.vs", "shaders/particle.fs", "particle");
	ResourceManager::LoadShader("shaders/post_processor.vs", "shaders/post_processor.fs", "post_processor");

	glm::mat4 projection = glm::ortho(0.0f, static_cast<float>(mWidth), static_cast<float>(mHeight), 0.0f, -1.0f, 1.0f);
	ResourceManager::GetShader("sprite").Use();
	ResourceManager::GetShader("sprite").SetInteger("image", 0);
	ResourceManager::GetShader("sprite").SetMatrix4("projection", projection);
	ResourceManager::GetShader("particle").Use();
	ResourceManager::GetShader("particle").SetInteger("image", 0);
	ResourceManager::GetShader("particle").SetMatrix4("projection", projection);

	ResourceManager::LoadTexture("textures/awesomeface.png", GL_TRUE, "face");
	ResourceManager::LoadTexture("textures/background.jpg", GL_FALSE, "background");
	ResourceManager::LoadTexture("textures/block.png", GL_FALSE, "block");
	ResourceManager::LoadTexture("textures/block_solid.png", GL_FALSE, "block_solid");
	ResourceManager::LoadTexture("textures/paddle.png", GL_TRUE, "paddle");
	ResourceManager::LoadTexture("textures/particle.png", GL_TRUE, "particle");
	ResourceManager::LoadTexture("textures/powerup_speed.png", GL_TRUE, "powerup_speed");
	ResourceManager::LoadTexture("textures/powerup_sticky.png", GL_TRUE, "powerup_sticky");
	ResourceManager::LoadTexture("textures/powerup_increase.png", GL_TRUE, "powerup_increase");
	ResourceManager::LoadTexture("textures/powerup_confuse.png", GL_TRUE, "powerup_confuse");
	ResourceManager::LoadTexture("textures/powerup_chaos.png", GL_TRUE, "powerup_chaos");
	ResourceManager::LoadTexture("textures/powerup_passthrough.png", GL_TRUE, "powerup_passthrough");

	pSpriteRenderer = new SpriteRenderer(ResourceManager::GetShader("sprite"));
	if (nullptr == pSpriteRenderer) {
		std::cout << "error::new pSpriteRenderer memory failed." << std::endl;
	}

	pParticles = new ParticleGenerator(ResourceManager::GetShader("particle"), ResourceManager::GetTexture("particle"), PARTICLE_AMOUNT);
	if (nullptr == pParticles) {
		std::cout << "error::new pParticles memory failed." << std::endl;
	}

	pPostProcessor = new PostProcessor(ResourceManager::GetShader("post_processor"), mWidth, mHeight);
	if (nullptr == pPostProcessor) {
		std::cout << "error::new pPostProcessor memory failed." << std::endl;
	}
	//pPostProcessor->SetConfuse(GL_TRUE);
	//pPostProcessor->SetChaos(GL_TRUE);
	//pPostProcessor->SetShaker(GL_TRUE);

	pTextRenderer = new TextRenderer(mWidth, mHeight);
	if (nullptr == pTextRenderer) {
		std::cout << "error::new pTextRenderer memory failed." << std::endl;
	}
	pTextRenderer->Load("fonts/OCRAEXT.TTF", 24);

	GameLevel one;
	one.Load("levels/one.lvl", mWidth, mHeight / 2);
	mLevels.push_back(one);

	GameLevel two;
	two.Load("levels/two.lvl", mWidth, mHeight / 2);
	mLevels.push_back(two);

	GameLevel three;
	three.Load("levels/three.lvl", mWidth, mHeight / 2);
	mLevels.push_back(three);

	GameLevel four;
	four.Load("levels/four.lvl", mWidth, mHeight / 2);
	mLevels.push_back(four);

	mLevel = 0;

	glm::vec2 playerPos = glm::vec2(mWidth / 2.0f - PLAYER_SIZE.x / 2.0f, mHeight - PLAYER_SIZE.y);
	pPlayer = new GameObject(playerPos, PLAYER_SIZE, ResourceManager::GetTexture("paddle"));
	if (nullptr == pPlayer) {
		std::cout << "error::new memory failed." << std::endl;
	}

	glm::vec2 ballPos = playerPos + glm::vec2(PLAYER_SIZE.x / 2 - BALL_RADIUS, -BALL_RADIUS * 2);
	pBall = new BallObject(ballPos, BALL_RADIUS, BALL_INIT_VELOCITY, ResourceManager::GetTexture("face"));
	if (nullptr == pBall) {
		std::cout << "error::new memory failed." << std::endl;
	}

	//pSoundEngine->play2D("audio/breakout.mp3", GL_TRUE);
	pSoundEngine->play2D("audio/MetalMax_unknownwilderness.mp3", GL_TRUE);
}

void Game::Update(GLfloat dt) {
	if (nullptr == pPlayer || nullptr == pSpriteRenderer || nullptr == pBall || nullptr == pParticles) {
		std::cout << "error::game::render nullptr." << std::endl;
		return;
	}

	pBall->Move(dt, mWidth);
	DoCollisions();
	pParticles->Update(dt, *pBall, 2, glm::vec2(pBall->GetRadius() / 2.0f));
	UpdatePowerUps(dt);

	if (ShakeTime > 0.0f) {
		ShakeTime -= dt;
		if (ShakeTime <= 0.0f) {
			pPostProcessor->SetShaker(GL_FALSE);
		}
	}

	if (pBall->GetPos().y >= mHeight) {
		--mLives;
		if (0 == mLives) {
			ResetLevel();
			mState = E_GAME_STATE::GAME_MENU;
		}
		ResetPlayer();
	}

	if (E_GAME_STATE::GAME_ACTIVE == mState && mLevels[mLevel].IsCompleted()) {
		ResetLevel();
		ResetPlayer();
		pPostProcessor->SetChaos(GL_TRUE);
		mState = E_GAME_STATE::GAME_WIN;
	}
}

void Game::ProcessInput(GLfloat dt) {
	if (nullptr == pPlayer || nullptr == pSpriteRenderer || nullptr == pBall || nullptr == pParticles) {
		std::cout << "error::game::render nullptr." << std::endl;
		return;
	}

	if (E_GAME_STATE::GAME_ACTIVE == mState) {
		GLfloat velocity = PLAYER_VELOCITY * dt;
		GLfloat playerX = pPlayer->GetPos().x;
		GLfloat ballX = pBall->GetPos().x;

		if (mKeys[GLFW_KEY_A]) {
			if (playerX >= 0.0f) {
				pPlayer->SetPosX(playerX - velocity);
				if (pBall->GetStuck()) {
					pBall->SetPosX(ballX - velocity);
				}
			}
		}

		if (mKeys[GLFW_KEY_D]) {
			if (playerX <= (mWidth - PLAYER_SIZE.x)) {
				pPlayer->SetPosX(playerX + velocity);
				if (pBall->GetStuck()) {
					pBall->SetPosX(ballX + velocity);
				}
			}
		}

		if (mKeys[GLFW_KEY_SPACE]) {
			pBall->SetStuck(false);
		}
	}

	if (E_GAME_STATE::GAME_MENU == mState) {
		if (mKeys[GLFW_KEY_ENTER] && !mKeysPressed[GLFW_KEY_ENTER]) {
			mState = E_GAME_STATE::GAME_ACTIVE;
			mKeysPressed[GLFW_KEY_ENTER] = GL_TRUE;
		}

		if (mKeys[GLFW_KEY_W] && !mKeysPressed[GLFW_KEY_W]) {
			mLevel = ( mLevel + 1 ) % 4;
			mKeysPressed[GLFW_KEY_W] = GL_TRUE;
		}

		if (mKeys[GLFW_KEY_S] && !mKeysPressed[GLFW_KEY_S]) {
			if (mLevel > 0) {
				--mLevel;
			}
			else {
				mLevel = 3;
			}
			mKeysPressed[GLFW_KEY_S] = GL_TRUE;
		}
	}

	if (E_GAME_STATE::GAME_WIN == mState) {
		if (mKeys[GLFW_KEY_ENTER]) {
			mKeysPressed[GLFW_KEY_ENTER] = GL_TRUE;
			pPostProcessor->SetChaos(GL_FALSE);
			mState = E_GAME_STATE::GAME_MENU;
		}
	}
}

void Game::Render() {
	if (nullptr == pPlayer || nullptr == pSpriteRenderer || nullptr == pBall || nullptr == pParticles) {
		std::cout << "error::game::render nullptr." << std::endl;
		return;
	}

	if (E_GAME_STATE::GAME_ACTIVE == mState || E_GAME_STATE::GAME_MENU == mState || E_GAME_STATE::GAME_WIN == mState) {
		// post processor
		pPostProcessor->BeginRender();
		// backgroud
		pSpriteRenderer->DrawSprite(ResourceManager::GetTexture("background"), glm::vec2(0.0f, 0.0f), glm::vec2(mWidth, mHeight), 0.0f);
		// level
		mLevels[mLevel].Draw(*pSpriteRenderer);
		// player
		pPlayer->Draw(*pSpriteRenderer);
		// powerup
		for (PowerUp& powerup : mPowerUps) {
			if (!powerup.GetDestroyed()) {
				powerup.Draw(*pSpriteRenderer);
			}
		}
		// particle
		pParticles->Draw();
		// ball
		pBall->Draw(*pSpriteRenderer);
		// post processor
		pPostProcessor->EndRender();
		pPostProcessor->Render(static_cast<GLfloat>(glfwGetTime()));

		std::stringstream ssLevels;
		ssLevels << mLevel;
		pTextRenderer->RenderText("Level:" + ssLevels.str(), 5.0f, 5.0f, 1.0f);

		std::stringstream ssLives;
		ssLives << mLives;
		pTextRenderer->RenderText("Lives:" + ssLives.str(), 5.0f, 25.0f, 1.0f);
	}

	if (E_GAME_STATE::GAME_MENU == mState) {
		pTextRenderer->RenderText("Press ENTER to start", 250.0f, mHeight / 2.0f, 1.0f);
		pTextRenderer->RenderText("Press W or S to select level", 245.0f, mHeight / 2.0f + 20.0f, 0.75f);
	}

	if (E_GAME_STATE::GAME_WIN == mState) {
		pTextRenderer->RenderText("You WON!!!", 320.0f, mHeight / 2.0f - 20.0f, 1.0f, glm::vec3(0.0f, 1.0f, 0.0f));
		pTextRenderer->RenderText("Press ENTER to retry or ESC to quit", 130.0f, mHeight / 2.0f, 1.0f, glm::vec3(1.0f, 1.0f, 0.0f));
	}
}

void Game::DoCollisions() {
	if (nullptr == pPlayer || nullptr == pSpriteRenderer || nullptr == pBall || nullptr == pParticles) {
		std::cout << "error::game::render nullptr." << std::endl;
		return;
	}

	// ball & box
	for (GameObject& box : mLevels[mLevel].GetBricks()) {
		if (!box.GetDestroyed()) {

			Collision collosionBB = CheckCollision(*pBall, box);
			if (std::get<0>(collosionBB)) {
				if (!box.GetIsSolid()) {
					if (box.GetColorLevel() == 2) {
						box.SetColorLevel(0);
						box.SetDestroyed(GL_TRUE);
					} 
					else if (box.GetColorLevel() > 2) {
						box.SetColorLevel(box.GetColorLevel() - 1);
					}
					SpawnPowerUps(box);
					pSoundEngine->play2D("audio/bleep.mp3", GL_FALSE);
				}
				else {
					ShakeTime = 0.05f;
					pPostProcessor->SetShaker(GL_TRUE);
					pSoundEngine->play2D("audio/solid.wav", GL_FALSE);
				}

				E_DIRECTION dir = std::get<1>(collosionBB);
				glm::vec2 diffVector = std::get<2>(collosionBB);
				if (!(pBall->GetPassThrough() && !box.GetIsSolid())) {
					if (E_DIRECTION::DIR_LEFT == dir || E_DIRECTION::DIR_RIGHT == dir) {
						pBall->SetVelocityX(-(pBall->GetVelocity().x));

						GLfloat penetration = pBall->GetRadius() - std::abs(diffVector.x);
						if (E_DIRECTION::DIR_LEFT == dir) {
							pBall->SetPosX(pBall->GetPos().x + penetration);
						}
						else {
							pBall->SetPosX(pBall->GetPos().x - penetration);
						}
					}
					else {
						pBall->SetVelocityY(-(pBall->GetVelocity().y));

						GLfloat penetration = pBall->GetRadius() - std::abs(diffVector.y);
						if (E_DIRECTION::DIR_UP == dir) {
							pBall->SetPosY(pBall->GetPos().y - penetration);
						}
						else {
							pBall->SetPosY(pBall->GetPos().y + penetration);
						}
					}
				}
			}
		}
	}

	for (PowerUp& powerup : mPowerUps) {
		if (!powerup.GetDestroyed()) {
			if (powerup.GetPos().y >= mHeight) {
				powerup.SetDestroyed(GL_TRUE);
			}

			if (CheckCollision(*pPlayer, powerup)) {
				powerup.SetDestroyed(GL_TRUE);
				powerup.SetActivated(GL_TRUE);
				ActivatePowerUp(powerup);
				pSoundEngine->play2D("audio/powerup.wav", GL_FALSE);
			}
		}
	}

	// ball & player
	Collision collisionBP = CheckCollision(*pBall, *pPlayer);
	if (!pBall->GetStuck() && std::get<0>(collisionBP)) {
		GLfloat centerBoard = pPlayer->GetPos().x + pPlayer->GetSize().x / 2.0f;
		GLfloat distance = (pBall->GetPos().x + pBall->GetRadius()) - centerBoard;
		GLfloat percentage = distance / (pPlayer->GetSize().x / 2.0f);

		GLfloat strength = 2.0f;
		glm::vec2 oldVelocity = pBall->GetVelocity();
		pBall->SetVelocityX(BALL_INIT_VELOCITY.x * percentage * strength);
		//pBall->SetVelocityY(-(pBall->GetVelocity().y));
		pBall->SetVelocity(glm::normalize(pBall->GetVelocity()) * glm::length(oldVelocity));
		pBall->SetVelocityY(-1 * std::abs(pBall->GetVelocity().y));
		pSoundEngine->play2D("audio/bleep.wav", GL_FALSE);

		pBall->SetStuck(pBall->GetSticky());
	}
}

GLboolean Game::CheckCollision(const GameObject& first, const GameObject& second) {
	// AABB - AABB collision
	GLboolean collisionX = ((first.GetPos().x + first.GetSize().x) >= (second.GetPos().x)) && ((second.GetPos().x + second.GetSize().x) >= (first.GetPos().x));
	GLboolean collisionY = ((first.GetPos().y + first.GetSize().y) >= (second.GetPos().y)) && ((second.GetPos().y + second.GetSize().y) >= (first.GetPos().y));
	return collisionX && collisionY;

	// AABB - Circle collision
	//glm::vec2 center(first.GetPos() + first.GetRadius());
	//glm::vec2 aabbHalfExtents(second.GetSize().x / 2, second.GetSize().y / 2);
	//glm::vec2 aabbCenter(second.GetPos().x + aabbHalfExtents.x, second.GetPos().y + aabbHalfExtents.y);
	//glm::vec2 difference = center - aabbCenter;
	//glm::vec2 clamped = glm::clamp(difference, -aabbHalfExtents, aabbHalfExtents);
	//glm::vec2 closest = aabbCenter + clamped;
	//difference = closest - center;
	//return glm::length(difference) < first.GetRadius();
}

Collision Game::CheckCollision(const BallObject& first, const GameObject& second) {
	//AABB - Circle collision
	glm::vec2 center(first.GetPos() + first.GetRadius());
	glm::vec2 aabbHalfExtents(second.GetSize().x / 2.0f, second.GetSize().y / 2.0f);
	glm::vec2 aabbCenter(second.GetPos().x + aabbHalfExtents.x, second.GetPos().y + aabbHalfExtents.y);
	glm::vec2 difference = center - aabbCenter;
	glm::vec2 clamped = glm::clamp(difference, -aabbHalfExtents, aabbHalfExtents);
	glm::vec2 closet = aabbCenter + clamped;
	difference = closet - center;

	if (glm::length(difference) < first.GetRadius()) {
		return std::make_tuple(GL_TRUE, VectorDirection(difference), difference);
	}
	else {
		return std::make_tuple(GL_FALSE, E_DIRECTION::DIR_UP, glm::vec2(0.0f, 0.0f));
	}
}

E_DIRECTION Game::VectorDirection(glm::vec2 target) {
	glm::vec2 compass[] = {
		glm::vec2(0.0f, 1.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(0.0f, -1.0f),
		glm::vec2(-1.0f, 0.0f),
	};

	GLfloat max = 0.0f;
	GLuint bestMatch = -1;
	for (GLuint i = 0; i < 4; ++i) {
		GLfloat dotProduct = glm::dot(glm::normalize(target), compass[i]);
		if (dotProduct > max) {
			max = dotProduct;
			bestMatch = i;
		}
	}
	return static_cast<E_DIRECTION>(bestMatch);
}


void Game::ResetLevel() {
	switch (mLevel) {
	case 0:
		mLevels[0].Load("levels/one.lvl", mWidth, mHeight / 2);
		break;

	case 1:
		mLevels[1].Load("levels/two.lvl", mWidth, mHeight / 2);
		break;

	case 2:
		mLevels[0].Load("levels/three.lvl", mWidth, mHeight / 2);
		break;

	case 3:
		mLevels[0].Load("levels/four.lvl", mWidth, mHeight / 2);
		break;

	default:
		break;
	}

	mLives = 3;
}

void Game::ResetPlayer() {
	if (nullptr == pPlayer || nullptr == pSpriteRenderer || nullptr == pBall) {
		std::cout << "error::game::render nullptr." << std::endl;
		return;
	}

	pPlayer->SetSize(PLAYER_SIZE);
	pPlayer->SetPos(glm::vec2(mWidth / 2.0f - PLAYER_SIZE.x / 2.0f, mHeight - PLAYER_SIZE.y));
	pBall->Reset(pPlayer->GetPos() + glm::vec2(PLAYER_SIZE.x / 2.0f - pBall->GetRadius(), -(pBall->GetRadius() * 2.0f)), BALL_INIT_VELOCITY);
}

void Game::SetKeys(GLuint index, GLboolean state) {
	if (0 > index || index > 1024) {
		std::cout << "error::SetKeys::key index out of range." << std::endl;
		return;
	}

	mKeys[index] = state;
}

void Game::SetKeysPressed(GLuint index, GLboolean state) {
	if (0 > index || index > 1024) {
		std::cout << "error::SetKeysPressed::key index out of range." << std::endl;
		return;
	}

	mKeysPressed[index] = state;
}

void Game::SpawnPowerUps(GameObject& block) {

	if (ShouldSpawn(RANDOM1_SPAWN)) {
		mPowerUps.push_back(PowerUp("speed", glm::vec3(0.5f, 0.5f, 1.0f), 0.0f, block.GetPos(), ResourceManager::GetTexture("powerup_speed")));
	}

	if (ShouldSpawn(RANDOM1_SPAWN)) {
		mPowerUps.push_back(PowerUp("sticky", COLOR_STICKY, 20.0f, block.GetPos(), ResourceManager::GetTexture("powerup_sticky")));
	}	
	
	if (ShouldSpawn(RANDOM1_SPAWN)) {
		mPowerUps.push_back(PowerUp("pass_through", COLOR_PASSTHROUGH, 10.0f, block.GetPos(), ResourceManager::GetTexture("powerup_passthrough")));
	}

	if (ShouldSpawn(RANDOM1_SPAWN)) {
		mPowerUps.push_back(PowerUp("pad_size_increase", glm::vec3(1.0f, 0.6f, 0.4f), 0.0f, block.GetPos(), ResourceManager::GetTexture("powerup_increase")));
	}

	if (ShouldSpawn(RANDOM2_SPAWN)) {
		mPowerUps.push_back(PowerUp("confuse", glm::vec3(1.0f, 0.3f, 0.3f), 15.0f, block.GetPos(), ResourceManager::GetTexture("powerup_confuse")));
	}

	if (ShouldSpawn(RANDOM2_SPAWN)) {
		mPowerUps.push_back(PowerUp("chaos", glm::vec3(0.9f, 0.25f, 0.25f), 15.0f, block.GetPos(), ResourceManager::GetTexture("powerup_chaos")));
	}

	std::cout << mPowerUps.size() << std::endl;
}

void Game::UpdatePowerUps(GLfloat dt) {
	for (PowerUp& powerup : mPowerUps) {
		powerup.SetPos( powerup.GetPos() +  dt * powerup.GetVelocity());
		if (powerup.GetActivated()) {
			powerup.SetDuration(powerup.GetDuration() - dt);

			if (powerup.GetDuration() <= 0.0f) {
				powerup.SetActivated(GL_FALSE);

				if ("sticky" == powerup.GetType()) {
					if (!IsOtherPowerUpActive(mPowerUps, "sticky")) {
						pBall->SetSticky(GL_FALSE);
						pPlayer->SetColor(glm::vec3(1.0f));
					}
				}
				else if ("pass_through" == powerup.GetType()) {
					if (!IsOtherPowerUpActive(mPowerUps, "pass_through")) {
						pBall->SetPassThrough(GL_FALSE);
						pPlayer->SetColor(glm::vec3(1.0f));
					}
				}
				else if ("confuse" == powerup.GetType()) {
					if (!IsOtherPowerUpActive(mPowerUps, "confuse")) {
						pPostProcessor->SetConfuse(GL_TRUE);
					}
				}
				else if ("chaos" == powerup.GetType()) {
					if (!IsOtherPowerUpActive(mPowerUps, "chaos")) {
						pPostProcessor->SetChaos(GL_TRUE);
					}
				}

			}
		}
	}
	mPowerUps.erase(std::remove_if(mPowerUps.begin(), mPowerUps.end(), 
		[](const PowerUp& powerup) { return  powerup.GetDestroyed() && !powerup.GetActivated(); }), mPowerUps.end());
}

GLboolean Game::ShouldSpawn(GLuint chance) {
	GLuint random = rand() % chance;
	return random == 0;
}

void Game::ActivatePowerUp(PowerUp& powerUp) {
	if (E_GAME_STATE::GAME_ACTIVE == mState) {
		if ("speed" == powerUp.GetType()) {
			pBall->SetVelocity(1.2f * pBall->GetVelocity());
		}
		else if ("sticky" == powerUp.GetType()) {
			pBall->SetSticky(GL_TRUE);
			pPlayer->SetColor(COLOR_STICKY);
		}
		else if ("pass_through" == powerUp.GetType()) {
			pBall->SetPassThrough(GL_TRUE);
			pPlayer->SetColor(COLOR_PASSTHROUGH);
		}
		else if ("pad_size_increase" == powerUp.GetType()) {
			pPlayer->SetSize(glm::vec2(pPlayer->GetSize().x + 50, pPlayer->GetSize().y));
		}
		else if ("confuse" == powerUp.GetType()) {
			if (!pPostProcessor->GetConfuse()) {
				pPostProcessor->SetConfuse(GL_TRUE);
			}
		}
		else if ("chaos" == powerUp.GetType()) {
			if (!pPostProcessor->GetChaos()) {
				pPostProcessor->SetChaos(GL_TRUE);
			}
		}
	}
}

GLboolean Game::IsOtherPowerUpActive(std::vector<PowerUp>& powerUps, std::string type) {
	for (const PowerUp& powerUp : powerUps) {
		if (powerUp.GetActivated() && powerUp.GetType() == type) {
			return true;
		}		
	}		
	return false;
}