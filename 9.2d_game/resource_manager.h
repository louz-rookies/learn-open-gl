#ifndef __2DGAME_RESOURCE_MANAGER_H__
#define __2DGAME_RESOURCE_MANAGER_H__

#include <map>
#include <string>

#include <glad/glad.h> // don't ignore

#include "texture.h"
#include "shader.h"

class ResourceManager {
public:
	static Shader LoadShader(const GLchar* vShaderFile, const GLchar* fShaderFile, std::string name);
	static Shader GetShader(std::string name);

	static Texture2D LoadTexture(const GLchar* file, bool alpha, std::string name);
	static Texture2D GetTexture(std::string name);

	static void Clear();
private:
	ResourceManager();

	static Shader LoadShaderFromFile(const GLchar* vShaderFile, const GLchar* fShaderFile);
	static Texture2D LoadTextureFromFile(const GLchar* file, bool alpha);

public:
	static std::map<std::string, Shader> sShaders;
	static std::map<std::string, Texture2D> sTextures;
};

#endif